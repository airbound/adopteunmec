<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Profil_Badge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\Profil;
use AppBundle\Form\ProfilType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @Route("/user/profil")
 */
class ProfilController extends Controller
{
    /**
     * @Route("/edit", name="profil.edit")
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $profil = $user->getProfil();
        
        
        if($profil == null)
            $profil = new Profil();


        $last_img = $profil->getImg();
        $userBadges = $em->getRepository('AppBundle:Profil_Badge')->findBy(['profil'=>$profil]);


        $srcImg = $profil->getImg()===null ? '/assets/profils/pictures/default.png' :'/assets/profils/pictures/'.$profil->getImg();



        $form = $this->createForm(ProfilType::class, $profil);
        $form->handleRequest($request);


        if ($form->isValid() && $form->isSubmitted()) {
            $directory = $this->getParameter('picture_directory');
            $cv_dir = $this->getParameter('cv_directory');
            $img = $form['img']->getData();
            $cv = $form['pdfCv']->getData();
            if(isset($img)){
                $extension = $img->guessExtension();
                $imgname = $profil->getId().'.'.$extension;
                $img->move($directory, $imgname);
                $profil->setImg($imgname);
            }
            if(isset($cv)){
                $extension = $cv->guessExtension();
                $cvname = $profil->getId().'.'.$extension;
                $cv->move($cv_dir, $cvname);
                $profil->setPdfCv($cvname);
            }

            $profil->setAdopted(false);
            $profil->setUser($user);
            $cv = $form['pdfCv']->getData();
            if ($cv) {
                $originalFilename = pathinfo($cv->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$cv->guessExtension();

                // Move the file to the directory where cv are stored
                try {
                    $cv->move(
                        $this->getParameter('cv_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $profil->setPdfCv($newFilename);
            }
            
            $pictures = $form['img']->getData();
            dump($pictures);
            if ($pictures) {
                $originalFilename = pathinfo($pictures->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$pictures->guessExtension();

                // Move the file to the directory where cv are stored
                try {
                    $pictures->move(
                        $this->getParameter('photo_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $profil->setImg($newFilename);
            }
            $em->persist($profil);
            $user->setProfil($profil);
            $em->persist($user);
            $em->flush();
            $srcImg = $profil->getImg()===null ? '/assets/profils/pictures/default.png' :'/assets/profils/pictures/'.$profil->getImg();
        }
        $form = $form->createView();

        return $this->render('@App/Profil/edit.html.twig', [
            'form' => $form,
            'srcImg' => $srcImg,
            'badges' => $this->getAllBadges(),
            'userBadges' => $userBadges,
            'profilId' => $profil->getId()
        ]);
    }

    /**
     * @Route("/edit/badge/add", name="profil.edit.badge.add")
     */
    public function editBadgeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $badge = $em->getRepository('AppBundle:Badge')->find($request->get('idBadge'));
        $profil = $em->getRepository('AppBundle:Profil')->find($request->get('idProfil'));
        $badgeProfil = new Profil_Badge();
        $badgeProfil->setBadge($badge);
        $badgeProfil->setProfil($profil);
        $badgeProfil->setEnable(true);
        $em->persist($badgeProfil);
        $em->flush();
        return new JsonResponse(
            [
                'status'=>200,
                'response' => [
                    'img' => '/assets/badges/'.$badge->getImg(),
                    'name' => $badge->getName()
                ]
                ]
        );
    }

    private function getAllBadges()
    {
        $em = $this->getDoctrine()->getManager();
        $badges = $em->getRepository('AppBundle:Badge')->findAll();
        $badges_array = [];
        foreach ($badges as $badge){
            array_push($badges_array, [
                'id' => $badge->getId(),
                'name' => $badge->getName(),
                'img' => '/assets/badges/'.$badge->getImg()
            ]);
        }
        return $badges_array;
    }

}
